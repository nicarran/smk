# Royal Kludge RK61

**Warning:** Be aware that there are several versions of the RK61 keyboard, each containing distinct internal components. This SMK firmware has been successfully tested on a wired 2022 model (with the date stamped on the board: 2022/08/16) and is likely to not function correctly on other variations. If you opt to install this firmware on your RK61, it is highly recommended to create a backup of your original firmware using the sinowealth-kb-tool [available here](https://github.com/carlossless/sinowealth-kb-tool) first. Keep in mind that you should know how to revert to the original firmware using sinowealth-kb-tool in case the SMK firmware does not work as intended. We disclaim any responsibility for any damage caused while trying this firmware or using the sinowealth-kb-tool.

## Keyboard Specs

- MCU: BYK916 (SH68F90?)
- Backlight: RGB LEDs
- Indicators: none
- Switches: none
- Wireless: wired only version
- Wireless: none, wired only version

## SMK Supported Features

- [x] Key Scan
- [ ] RGB Matrix
